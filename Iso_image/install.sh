#!/bin/bash

##
#   Names: Ismael, Hamza 
#   Date: 21/01/2014
##

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts

#To avoid locale issues and in order to import GPG keys
export HOME=/root
export LC_ALL=C

##########Packages installation####################

#retrieval of list of packages to install
echo -n "Enter the list of packages you want to install, separated by spaces "
read list_packages
echo "Packages list to install: $list_packages"


echo "deb http://fr.archive.ubuntu.com/ubuntu/ saucy universe" >> /etc/apt/sources.list
echo "deb http://security.ubuntu.com/ubuntu saucy-security universe" >> /etc/apt/sources.list


#update and install
sudo apt-get update
sudo apt-get install $list_packages

#Cleanup after installation

sudo apt-get aptitude clean

rm -rf /tmp/* ~/.bash_history

rm /etc/hosts

rm /etc/resolv.conf

#rm /var/lib/dbus/machine-id

#rm /sbin/initctl
#dpkg-divert --rename --remove /sbin/initctl

#from within the chroot environment.
#unmount special filesystems and exit chroot
umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
exit
