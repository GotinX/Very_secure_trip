#!/bin/bash

##
#   Names: Ismael, Hamza 
#   Date: 21/01/2014
##

if [ $# -ne 2 ]; then
    echo "Usage: $0 Iso_image Output_name.iso"
    exit 1
fi

#########Extract the CD .iso contents##############

#Mount the Desktop .iso
mkdir -p mnt 
sudo mount -o loop $1 mnt

#Extract .iso contents into dir 'extract-cd'
mkdir -p extract-cd
sudo rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract-cd

#Extract the SquashFS filesystem
sudo unsquashfs mnt/casper/filesystem.squashfs
sudo mv squashfs-root edit

########Prepare and chroot########################

#network connection within chroot
sudo rm edit/etc/resolv.conf
sudo cp /etc/resolv.conf edit/etc/
sudo cp /etc/apt/apt.conf edit/etc/apt/

#Depending on your configuration, you may also need to copy the hosts file
sudo cp /etc/hosts edit/etc/
sudo cp install.sh edit

sudo mount --bind /dev/ edit/dev
sudo chroot edit /install.sh

sudo umount edit/dev/


###########Producing the CD image#########################""

#Regenerate manifest 
chmod +w extract-cd/casper/filesystem.manifest
sudo chroot edit dpkg-query -W --showformat='${Package} ${Version}\n' > extract-cd/casper/filesystem.manifest
sudo cp extract-cd/casper/filesystem.manifest extract-cd/casper/filesystem.manifest-desktop
sudo sed -i '/ubiquity/d' extract-cd/casper/filesystem.manifest-desktop
sudo sed -i '/casper/d' extract-cd/casper/filesystem.manifest-desktop

#Compress filesystem
sudo mksquashfs edit extract-cd/casper/filesystem.squashfs -b 1048576

#Update the filesystem.size file, which is needed by the installer:
printf $(sudo du -sx --block-size=1 edit | cut -f1) > extract-cd/casper/filesystem.size


#Remove old md5sum.txt and calculate new md5 sums
cd extract-cd
sudo rm md5sum.txt
find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt

IMAGE_NAME="Mon_iso"
#Create the ISO image
sudo mkisofs -D -r -V "$IMAGE_NAME" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../$2 .

#Cleaning
cd ..
sudo umount mnt/
sudo rm -r mnt/
sudo rm -r extract-cd/
sudo rm -r edit/



