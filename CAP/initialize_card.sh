#!/bin/bash

version=$(opensc-tool -i | head -1 | awk '{print $2}')
if [[ "$version" != "0.12.2" ]]; then
   echo "Installing Opensc 0.12.2 ..."
   unzip OpenSC.zip > /dev/null 2>&1
   cd OpenSC
   ./bootstrap > /dev/null 2>&1
   LIBS=-lltdl ./configure --prefix=/usr --sysconfdir=/etc/opensc > /dev/null 2>&1
   make > /dev/null 2>&1
   sudo make install > /dev/null 2>&1
   cd ..
   rm -R OpenSC
fi

sudo cp pkcs15.profile /usr/share/opensc/

opensc-tool -w -n
if [ "$?" != "0" ]; then
    exit 1
fi 

echo "Deleting the old applet ..."
gpshell < delete_musclecard.txt > /dev/null 2>&1

echo "Installing the new applet ..."
gpshell < install_musclecard.txt > /dev/null 2>&1

echo "Initializing the applet ..."
opensc-tool -s 00:A4:04:00:06:A0:00:00:00:01:01 -s B0:2A:00:00:38:08:4D:75:73:63:6C:65:30:30:04:01:08:30:30:30:30:30:30:30:30:08:30:30:30:30:30:30:30:30:05:02:08:30:30:30:30:30:30:30:30:08:30:30:30:30:30:30:30:30:00:00:17:70:00:02:01 > /dev/null 2>&1

PIN="00000000"

read -p "Please initialize User PUK [12345678]: "
PUK=$REPLY

if [[ "$PUK" == "" ]]; then
	PUK="12345678"
fi

len=$(expr length "$PUK")
while [ $len -lt 8 ]
do
   read -p "The PUK must have 8 chars or more: "
   PUK=$REPLY
   len=$(expr length "$PUK")
done

echo "Creating the structure of pkcs#15 on the card and initializing pin and puk code ..."
pkcs15-init -C -p pkcs15+onepin --pin $PIN --puk $PUK > /dev/null 2>&1
if [ "$?" != "0" ]; then
    echo "Error while creating the structure of pkcs#15 on the card and initializing pin and puk code"
    exit 2
fi

read -p "Please insert certificate path [cert.crt]: "
path=$REPLY

if [[ "$path" == "" ]]; then
	path=$(pwd)"/cert.crt"
fi

while [ ! -e $path ]
do
   read -p "The path is incorrect: "
   path=$REPLY
done

echo "Storing certificate on the card ..."
pkcs15-init --auth-id 1 --store-certificate $path --pin $PIN  > /dev/null 2>&1
if [ "$?" != "0" ]; then
    echo "Error while storing certificate on the card"
    exit 3
fi

read -p "Please insert private key path [key.pem]: "
path=$REPLY

if [[ "$path" == "" ]]; then
	path=$(pwd)"/key.pem"
fi

while [ ! -e $path ]
do
   read -p "The path is incorrect: "
   path=$REPLY
done

echo "Storing the private key on the card ..."
pkcs15-init --auth-id 1 --store-private-key $path --pin $PIN > /dev/null 2>&1
if [ "$?" != "0" ]; then
    echo "Error while storing the private key on the card"
    exit 4
fi

echo "Changing pin code ..."
pkcs15-tool --change-pin --pin $PIN
if [ "$?" != "0" ]; then
    echo "Error while changing pin code"
    exit 5
fi

exit 0

