#!/usr/bin/expect -f

set timeout 10
set login [lindex $argv 0]
set pin [lindex $argv 1]
set host [lindex $argv 2]
set filename [lindex $argv 3]
set buffer [open $filename "w"]


spawn openvpn --config /etc/config/client.conf --remote $host 1594
expect {
	timeout {puts $buffer 2; close $buffer; exit 1}
	"Enter Auth Username:"
}

send "$login\r";

expect {
	timeout {puts $buffer 2; close $buffer; exit 1}
	"Enter Auth Password:"
}

send "$pin\r";

expect {
	timeout {puts $buffer 1; close $buffer; exit 1}
	"Initialization Sequence Completed"
 }

puts $buffer 0; 
close $buffer;

interact

