#!/usr/bin/expect -f

set timeout 15
set filename "return.txt"
set buffer [open $filename "w"]
set pin [lindex $argv 0]
set host [lindex $argv 1]
cd /etc/openvpn

spawn openvpn --config client.conf --remote $host
expect {
	timeout {puts $buffer 2; close $buffer; exit 1}
	"Enter MUSCLE (User PIN) token Password:"
}

send "$pin\r";

expect {
	timeout {puts $buffer 1; close $buffer; exit 1}
	"Initialization Sequence Completed"
 }

puts $buffer 0; close $buffer;

interact

