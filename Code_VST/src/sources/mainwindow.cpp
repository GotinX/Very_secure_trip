/*
*   Names: Hamza
*   Date: 22/01/2014
*/

#include "mainwindow.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSizePolicy>
#include <QTextEdit>
#include <QProcess>
#include <QMessageBox>
#include <QDateTime>
#include <QProgressBar>
#include <QProgressDialog>
#include <QAction>

// CONSTRUCTOR
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
    createModel();
    createView();
    createMenuBar();
    placeMenuBar();
    placeComponents();
    createController();

    setIsLogin(false);
}

// DESTRUCTOR
MainWindow::~MainWindow() {
}

// FUNCTION : Create model
void MainWindow::createModel() {
}

// FUNCTION : Create window
void MainWindow::createView() {
    this->setWindowTitle(QString::fromUtf8("Very Secure Trip v1.0"));
    //this->showMaximized();
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinMaxButtonsHint);

    centralWidget = new QWidget(this);
    filesView1 = new FilesView(QBool(false), QDir::homePath()+"/workspace");
    filesView2 = new FilesView(QBool(true), "inf-srv-ssi2secu.univ-rouen.fr");
    toolBar = new QToolBar();
}

// FUNCTION : Create toolbar
void MainWindow::createMenuBar() {
    menuBar = new QMenuBar();
    getData = new QAction("Get remote data", this);
}

// FUNCTION : update toolbar
void MainWindow::placeMenuBar() {
    QAction *about = new QAction("About",this);
    menuBar->addAction(about);
    connect(about,SIGNAL(triggered()),this,SLOT(about()));
    this->setMenuBar(menuBar);
}


// SLOT : call when click on bar "about"
void MainWindow::about(){
    QMessageBox::about(this,"About","Software developed by : \n" \
                       "    - Charles ANGO \t angocharles@gmail.com\n" \
                       "    - Mathieu FIGUERES \t matfig66@gmail.com\n" \
                       "    - Ismael KABORE \t ambroiseismael@gmail.com\n" \
                       "    - Vincent ROSET \t vistrate360@gmail.com\n" \
                       "    - Hamza ZERHOUNI \t zerhounihamza@gmail.com\n" \
                       "\n For :\n" \
                       "    - Magali BARDET\n" \
                       "    - Ayoub OTMANI\n" \
                       "\n Master degree project 2013-2014 - Rouen University\n" \
                       "\n LICENSE : LGPL V3, 29 June 2007");
}

// FUNCTION : icons bar
void MainWindow::placeComponents() {
    QVBoxLayout *layout = new QVBoxLayout();
    QHBoxLayout *layouth1 = new QHBoxLayout();
    QHBoxLayout *layouth2 = new QHBoxLayout();
    QVBoxLayout *layouthCentre = new QVBoxLayout();

    buttonDownload = new QPushButton();
    buttonDownload->setIcon(QIcon(":/left"));
    buttonDownload->setToolTip("Download selected files");
    buttonDownload->setEnabled(false);

    buttonUpload = new QPushButton("");
    buttonUpload->setIcon(QIcon(":/right"));
    buttonUpload->setToolTip("Upload selected files");
    buttonUpload->setEnabled(false);

    buttonBrowser = new QToolButton;
    buttonBrowser->setIcon(QIcon(":/browser"));
    buttonBrowser->setToolTip("Open browser");

    buttonEditor = new QToolButton;
    buttonEditor->setIcon(QIcon(":/editor"));
    buttonEditor->setToolTip("Open text editor");

    buttonPdf = new QToolButton;
    buttonPdf->setIcon(QIcon(":/readerpdf"));
    buttonPdf->setToolTip("Open pdf reader");

    buttonQuit = new QToolButton;
    buttonQuit->setIcon(QIcon(":/quit"));
    buttonQuit->setToolTip("Quit and shutdown computer");

    buttonTerm = new QToolButton;
    buttonTerm->setIcon(QIcon(":/console"));
    buttonTerm->setToolTip("Open command-line interface");

    toolBar->addWidget(buttonBrowser);
    toolBar->addWidget(buttonEditor);
    toolBar->addWidget(buttonPdf);
    toolBar->addWidget(buttonTerm);
    toolBar->addSeparator();
    toolBar->addWidget(buttonQuit);


    toolBar->setMovable(false);

    debugText = new QTextEdit();
    debugText->setReadOnly(true);
    debugText->setMaximumHeight(150);

    layouth1->addWidget(debugText);
    layouthCentre->addStretch(4);
    layouthCentre->addWidget(buttonDownload);
    layouthCentre->addWidget(buttonUpload);
    layouthCentre->addStretch(1);
    layouth2->addWidget(filesView1);
    layouth2->addLayout(layouthCentre);
    layouth2->addWidget(filesView2);
    layout->addLayout(layouth1);
    layout->addLayout(layouth2);
    centralWidget->setLayout(layout);

    this->setCentralWidget(centralWidget);
    this->addToolBar(toolBar);
}

// FUNCTION : Create controller
void MainWindow::createController() {
    connect(buttonDownload,SIGNAL(clicked()),this,SLOT(getDownloadList()));
    connect(buttonUpload,SIGNAL(clicked()),this,SLOT(getUploadList()));
    connect(buttonTerm,SIGNAL(clicked()),this,SLOT(openTerm()));
    connect(buttonBrowser,SIGNAL(clicked()),this,SLOT(openBrowser()));
    connect(buttonEditor,SIGNAL(clicked()),this,SLOT(openEditor()));
    connect(buttonPdf,SIGNAL(clicked()),this,SLOT(openPdf()));
    connect(buttonQuit,SIGNAL(clicked()),this,SLOT(quitter()));
    connect(filesView2,SIGNAL(textToMainWindow(QString)),this,SLOT(TextFromFtp(QString)));
    connect(filesView2,SIGNAL(connected()),this,SLOT(getConnected()));
    connect(filesView2,SIGNAL(disconnected()),this,SLOT(getDisconnected()));
}

// SLOT : when button clicked : for open evince (pdf)
void MainWindow::openPdf(){
    QProcess *monProg = new QProcess();
    QString prg_cons = "/usr/bin/evince";
    monProg->start(prg_cons);
}

// SLOT : when button clicked : for open terminal
void MainWindow::openTerm() {
    QProcess *monProg = new QProcess();
    QString prg_cons = "/usr/bin/launchConsole.sh";
    monProg->start(prg_cons);
}

void MainWindow::openTermTest() {
    QProcess *monProg = new QProcess();
    QStringList *arguList = new QStringList;
    QString prg_cons = "/usr/bin/xterm";
    arguList->append("-e");
    arguList->append("/bin/bash");
    monProg->start(prg_cons,*arguList);
    //monProg->execute("/usr/bin/xterm");
}

// SLOT : when button clicked : for open browser
void MainWindow::openBrowser(){
    QProcess *monProg = new QProcess();
    QStringList *arguList = new QStringList;
    QString prg_cons = "/usr/bin/midori";
    arguList->append("--private");
    arguList->append("https://www.google.fr");
    monProg->start(prg_cons,*arguList);
}

// SLOT : when button clicked : for open texte editor
void MainWindow::openEditor(){
    QProcess *monProg = new QProcess();
    QString prg_cons = "/usr/bin/leafpad";
    monProg->start(prg_cons);
}

// SLOT : when button clicked : for quit distribution
void MainWindow::quitter(){
    // effacement données via shred dans le script
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Shutdown","Are you sure ?\nThis will remove all your local data.",QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes){
        QProcess *monProg = new QProcess();
        QStringList *arguList = new QStringList;
        QString prg_cons = "/etc/scripts/shutdown.sh";
        arguList->append(QDir::homePath()+"/.workspace");
        arguList->append(QDir::homePath()+"/workspace");
        monProg->start(prg_cons,*arguList);

        QProgressDialog *erase_loading = new QProgressDialog("Deleting local data...", "Cancel", 0, 0);
        erase_loading->setCancelButtonText(0);
        erase_loading->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        erase_loading->exec();
    }
}

// SLOT : for display message
void MainWindow::TextFromFtp(QString str) {
    QTime time = QTime::currentTime();
    debugText->append(time.toString("H:m:s") + " : " + str);
}

// SLOT : when button clicked : get files list and download
void MainWindow::getDownloadList() {
    if(isLogin){
        QMessageBox::StandardButton reply;

        sortListFtp = filesView2->downloadList();
        if(sortListFtp.empty()){
            QMessageBox::information(this,"Error","Any selected data");
        }else{
            reply = QMessageBox::question(this,"Download","Dowload this files",QMessageBox::Yes|QMessageBox::No);
            if (reply==QMessageBox::Yes){
                filesView2->callDownload(sortListFtp);
                filesView2->EmptyFtprList();
            }
        }
        sortListFtp.clear();
    }else{
        QMessageBox::information(this,"Error","You are currently not connected to the FTP");
    }
}

// SLOT : update var isLogin
void MainWindow::getConnected(){
    setIsLogin(true);
}

// SLOT : update var isLogin
void MainWindow::getDisconnected(){
    setIsLogin(false);
}

// FUNCTION : update var isLogin
void MainWindow::setIsLogin(bool b){
    isLogin = b;
    buttonDownload->setEnabled(b);
    buttonUpload->setEnabled(b);
}

// FUNCTION : update var isLogin
bool MainWindow::getIsLogin(){
    return isLogin;
}

// SLOT : when button clicked : get files list and upload
void MainWindow::getUploadList(){
    if(isLogin){
        QMessageBox::StandardButton reply;

        if(filesView1->UploadList().empty()){
            QMessageBox::information(this,"Error","Any selected data");
        }else{
            reply = QMessageBox::question(this,"Upload","Upload this files",QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes) {
                filesView2->upload(filesView1->UploadList());
                filesView1->EmptyUserList();
            }
        }

    } else {
        QMessageBox::information(this,"Error","You are currently not connected to the FTP");
    }
}
