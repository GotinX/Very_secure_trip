#include "progressbar.h"

#include <QVBoxLayout>
#include <QLabel>

// CONSTRUCTOR
ProgressBar::ProgressBar(QString fileName, QWidget *parent) :
    QWidget(parent) {

    this->setWindowTitle("Transfer in progress ...");
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    progressBar = new QProgressBar();
    QVBoxLayout *layout = new QVBoxLayout();

    this->fileName = fileName;

    layout->addWidget(new QLabel("File: " + this->fileName), Qt::AlignCenter);
    layout->addWidget(progressBar);

    setLayout(layout);
    setWindowModality(Qt::WindowModal);
}

// SLOT : set size file
void ProgressBar::total(quint64 total) {
    progressBar->setMaximum(total);
}

// SLOT : set progress value
void ProgressBar::done(quint64 done) {
    progressBar->setValue(done);
}

// FUNCTION : get file name in transfer
QString ProgressBar::getFile() {
    return fileName;
}
