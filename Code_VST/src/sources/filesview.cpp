/*
*   Names: Hamza
*   Date: 22/01/2014
*/

#include <QTreeWidgetItem>
#include <QDir>
#include <QVBoxLayout>
#include <QDebug>
#include <QMenu>
#include <QListWidget>
#include <QStandardItemModel>
#include <QInputDialog>
#include <QDialog>
#include <QMessageBox>
#include <QProcess>
#include <QDir>
#include <QModelIndexList>
#include <QDirModel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QLabel>
#include <QMimeData>
#include <QProgressDialog>
#include <QTimer>

#include "filesview.h"

// CONSTRUCTOR
FilesView::FilesView(QBool isFtp, QString path, QWidget *parent) :
    QWidget(parent) {

    flagFTP = false;
    tree = new QTreeView();
    list = new ListView();
    QVBoxLayout *vLayout = new QVBoxLayout();
    pathLine = new QLineEdit(path);
    flagLogin = false;
    loggedIn = false;
    ftpModel = new FtpModel();
    progressBar = new ProgressBar();

    if (isFtp) {
        loginDialog = new LoginDialog(this);
        otpLogin = new OTPLoginDialog(this);
        ftpLogin = "";
        ftpPassword = "";
        ftpOtp = "";
        tree->setModel(ftpModel);
        list->setSelectionMode(QAbstractItemView::ExtendedSelection);

        QHBoxLayout *hLayout = new QHBoxLayout();
        buttonConnect = new QPushButton("Connect");
        hLayout->addWidget(new QLabel("Host"));
        hLayout->addWidget(pathLine);
        hLayout->addWidget(buttonConnect);
        vLayout->addLayout(hLayout);

        connect(buttonConnect, SIGNAL(clicked()), this, SLOT(FtpConnect()));
        connect(otpLogin, SIGNAL(acceptLogin(QString, QString, QString, QBool)),this, SLOT(slotAcceptUserLogin(QString, QString, QString, QBool)));
        connect(loginDialog, SIGNAL(acceptLogin(QString, QString, QBool)),this, SLOT(slotAcceptUserLogin(QString, QString, QBool)));
        connect(ftpModel, SIGNAL(textToFilesView(QString)), this, SLOT(textFromFtpModel(QString)));
        connect(ftpModel, SIGNAL(disconnectToFilesView()), this, SLOT(disconnectFromFtpModel()));
        connect(ftpModel, SIGNAL(connectToFilesView()), this, SLOT(connectFromFtpModel()));
        connect(tree, SIGNAL(clicked(QModelIndex)), this, SLOT(OnTreeClickedFTP(QModelIndex)));
        connect(list, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(userListFtpMenuRequested(QPoint)));
        connect(list, SIGNAL(pressed(QModelIndex)), this, SLOT(OnListFtpClicked(QModelIndex)));

        tree->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(tree, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotMenuTestFtp(QPoint)));
        connect(ftpModel, SIGNAL(TransfertProgress(qint64,qint64,QList<QString>*, bool)), this, SLOT(TransfertProgressFromFtpModel(qint64, qint64, QList<QString>*, bool)));
    } else {
        pathLine->setEnabled(false);

        dirModel = new QFileSystemModel(this);
        dirModel->setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
        dirModel->setRootPath(path);
        tree->setModel(dirModel);

        QModelIndex idx = dirModel->index(path);
        tree->setRootIndex(idx);

        fileModel = new QFileSystemModel(this);
        fileModel->setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
        fileModel->setRootPath(path);

        list->setSelectionMode(QAbstractItemView::ExtendedSelection);
        vLayout->addWidget(pathLine);

        connect(tree, SIGNAL(clicked(QModelIndex)), this, SLOT(OnTreeClicked(QModelIndex)));
        connect(list, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(userListMenuRequested(QPoint)));
        connect(list, SIGNAL(pressed(QModelIndex)), this, SLOT(OnListClicked(QModelIndex)));
        connect(pathLine, SIGNAL(textChanged(QString)), this, SLOT(pathChanged(QString)));
        tree->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(tree, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotMenuTest(QPoint)));
    }

    vLayout->addWidget(tree);
    vLayout->addWidget(list);
    this->setLayout(vLayout);
    list->setContextMenuPolicy(Qt::ActionsContextMenu);
    list->setSelectionBehavior(QAbstractItemView::SelectRows);
}

// SLOT : when tree FTP is clicked
void FilesView::OnTreeClickedFTP(QModelIndex index) {
    Q_UNUSED(index);
    flagFTP = true;
}

// SLOT : when right clic FTP : action (remove, add list)
void FilesView::slotMenuTestFtp(QPoint pos) {
    if(!flagFTP) return;

    QMenu myMenu;
    myMenu.addAction("Add to send list");
    myMenu.addSeparator();
    myMenu.addAction("Remove");
    myMenu.addSeparator();
    myMenu.addAction("Cut");
    if(ftpModel->getCopy()){
        myMenu.addAction("Paste");
    }
    QPoint globalPos = tree->mapToGlobal(pos);
    QAction* selectedItem = myMenu.exec(globalPos);

    QModelIndex index = tree->currentIndex();
    QString sPath = ftpModel->filePath(index);

    if (selectedItem){
        QString choice = selectedItem->text();
        if (choice == "Cut"){
            if (ftpModel->isDir(index)){
                emit textToMainWindow("Error : Can't copy folder.");
            } else {
                ftpModel->setCutMemory(sPath);
                ftpModel->setCopy(true);
            }
        } else if (choice == "Paste") {
            if (ftpModel->isDir(index)){
                sPath+="/";
                ftpModel->cutAndCopy(sPath);
            } else {
                QRegExp rxreplace("[^/]*$");
                sPath.replace(rxreplace,"");
                ftpModel->cutAndCopy(sPath);
            }
        } else if (choice == "Remove"){
            QMessageBox::StandardButton reply;
            if (ftpModel->isDir(index)){
                reply = QMessageBox::question(this,"Remove","Are you sure to remove this directory (and sub-files/directories) ?",QMessageBox::Yes|QMessageBox::No);
                if (reply == QMessageBox::Yes) {
                    // true = dir
                    ftpModel->removeToFtp(sPath,true);
                    ftpModel->refresh(tree->rootIndex());
                }
            } else {
                reply = QMessageBox::question(this,"Remove","Are you sure to remove this file ?",QMessageBox::Yes|QMessageBox::No);
                if (reply == QMessageBox::Yes) {
                    // false = file
                    ftpModel->removeToFtp(sPath,false);
                    ftpModel->refresh(tree->rootIndex());
                }
            }
        } else if (choice == "Add to send list") {
            if (!ftpModel->isDir(index)) {
                ftpMap.insert(ftpModel->filePath(index), ftpModel->filePath(index));
                list->addItem("[F]\t" + ftpModel->filePath(index));
            } else {
                ftpMap.insertMulti(ftpModel->filePath(index), ftpModel->filePath(index));
                list->addItem("[D]\t" + ftpModel->filePath(index));
            }
        }
    }
    flagFTP = false;
}

// SLOT : when right clic : action (open with, remove, rename, create dir, add list)
void FilesView::slotMenuTest(QPoint pos) {
    QString lec_pdf ="/usr/bin/evince";
    QString lec_fic ="/usr/bin/leafpad";
    QString lec_zip ="/usr/bin/file-roller";
    QStringList *arguList = new QStringList;
    QProcess *monProg = new QProcess();
    QMenu myMenu;

    QModelIndex index = tree->currentIndex();
    QString sPath = dirModel->fileInfo(index).absoluteFilePath();
    QDir *dir = new QDir();
    QProcess p;
    p.start("file", QStringList() << sPath);
    QString mime;
    if (p.waitForStarted()){
       p.waitForFinished();
       mime = p.readAllStandardOutput();
    }

    if (dirModel->fileInfo(index).filePath() != ""){
        myMenu.addAction("Create directory");
        myMenu.addSeparator();
        myMenu.addAction("Add to send list");
        myMenu.addSeparator();
        if (mime.contains("text")){
            myMenu.addAction("Open with Leafpad");
        } else if (mime.contains("PDF")){
            myMenu.addAction("Open with Evince");
        } else if (mime.contains("Zip") || mime.contains("zip") || mime.contains("tar")){
            myMenu.addAction("Open with File-roller");
        }
        myMenu.addSeparator();
        myMenu.addAction("Rename");
    }

    QPoint globalPos = tree->mapToGlobal(pos);
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem){
        QString choice = selectedItem->text();
        if (choice == "Rename"){
            bool ok;
            QString text = QInputDialog::getText(this, tr("Rename"),tr("Specify the new name"), QLineEdit::Normal,"", &ok);
            if (ok && !text.isEmpty()){
                if(!dir->rename(sPath,dirModel->fileInfo(index).absolutePath() + "/" + text))
                    QMessageBox::question(this,"Error","Name already exist",QMessageBox::Ok);
            }
        }else if (choice == "Create directory"){
            bool ok;
            QString text = QInputDialog::getText(this, tr("Create directory"),tr("Specify the directory's name"), QLineEdit::Normal,"", &ok);
            if (ok && !text.isEmpty()){
                if (dirModel->fileInfo(index).isDir()){
                    if(!dir->mkdir(sPath + "/" + text))
                        QMessageBox::question(this,"Error","Name already exist",QMessageBox::Ok);
                }else{
                    if(!dir->mkdir(dirModel->fileInfo(index).absolutePath() + "/" + text))
                        QMessageBox::question(this,"Error","Name already exist",QMessageBox::Ok);
                }
            }
        } else if (choice == "Add to send list") {
            if (!dirModel->fileInfo(index).isDir()) {
                localMap.insert(dirModel->fileInfo(index).filePath(), dirModel->fileInfo(index));
                list->addItem("[F]\t" + dirModel->fileInfo(index).filePath());
            } else {
                localMap.insert(dirModel->fileInfo(index).filePath(), dirModel->fileInfo(index));
                list->addItem("[D]\t" + dirModel->fileInfo(index).filePath());
            }
        } else if (choice == "Open with Leafpad") {
            arguList->append(sPath);
            monProg->start(lec_fic,*arguList);
        } else if (choice == "Open with Evince") {
            arguList->append(sPath);
            monProg->start(lec_pdf,*arguList);
        } else if (choice == "Open with File-roller") {
            arguList->append(sPath);
            monProg->start(lec_zip,*arguList);
        }
    }
    arguList->clear();
}

// SLOT : when tree local is clicked
void FilesView::OnTreeClicked(QModelIndex index) {
    Q_UNUSED(index);
}

// SLOT : when tree FTP is clicked
void FilesView::OnListFtpClicked(QModelIndex index) {
    Q_UNUSED(index);
}

// SLOT : when right clic in FTP bottom view : action remove list
void FilesView::userListFtpMenuRequested(const QPoint & pos) {
    QMenu myMenu;
    myMenu.addAction("Remove from transfert list");
    QPoint globalPos = list->mapToGlobal(pos);
    QAction* selectedItem = myMenu.exec(globalPos);

    if (selectedItem){
        QString choice = selectedItem->text();
        if (choice == "Remove from transfert list") {
            foreach (QListWidgetItem *item, list->selectedItems()) {
                list->takeItem(list->row(item));
                ftpMap.remove(item->text().mid(4));
            }
        }
    }
}

// SLOT : when clic in local bottom view
void FilesView::OnListClicked(QModelIndex index) {
    Q_UNUSED(index);
}

// SLOT : when right clic in local bottom view
void FilesView::userListMenuRequested(QPoint pos) {
    QMenu myMenu;
    myMenu.addAction("Remove from transfert list");
    QPoint globalPos = list->mapToGlobal(pos);
    QAction* selectedItem = myMenu.exec(globalPos);

    if (selectedItem){
        QString choice = selectedItem->text();
        if (choice == "Remove from transfert list") {
            foreach (QListWidgetItem *item, list->selectedItems()) {
                list->takeItem(list->row(item));
                localMap.remove(item->text().mid(4));
            }
        }
    }
}

// FUNCTION : return list
QListView *FilesView::getList() {
    return list;
}

// FUNCTION : return tree
QTreeView* FilesView::getTree(){
    return tree;
}

// FUNCTION : get local model
QFileSystemModel *FilesView::getDirModel() {
    return dirModel;
}

// FUNCTION : get FTP model
FtpModel *FilesView::getFtpModel() {
    return ftpModel;
}

// SLOT : set tree path
void FilesView::pathChanged(QString s) {
    QModelIndex index = dirModel->index(s);
    tree->setRootIndex(index);
}

// SLOT : when button connect is clicked : start logindialog pop up
void FilesView::FtpConnect() {
    QMessageBox choiceAuth(QMessageBox::Question,trUtf8("Authentification"),trUtf8("Choice your authentification"),QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);

    choiceAuth.setButtonText(QMessageBox::Yes, trUtf8("Card"));
    choiceAuth.setButtonText(QMessageBox::No, trUtf8("OTP"));

    int reply = choiceAuth.exec();

    if (reply == QMessageBox::Yes) {
        // Card
        flagCard = true;
        QProcess *monProg = new QProcess();
        QStringList *arguList = new QStringList;
        QProgressDialog *msgBox = new QProgressDialog("Insert your card", "Cancel", 0, 0);

        connect(monProg,SIGNAL(finished(int)),msgBox,SLOT(close()));

        monProg->start("/usr/bin/opensc-tool -w -n");
        msgBox->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        msgBox->setCancelButtonText(0);
        QPushButton *cancel = new QPushButton("Cancel");
        connect(cancel,SIGNAL(clicked()),this,SLOT(setFlagInsertCard()));
        msgBox->setCancelButton(cancel);
        msgBox->exec();

        if(flagCard){
            loginDialog->exec();
            if (flagLogin){
                QProcess *monProgPIN = new QProcess();

                QTimer* timer = new QTimer;
                QTimer* timerForHide = new QTimer;
                timer->setSingleShot(true);
                timerForHide->setSingleShot(true);
                connect(timer,SIGNAL(timeout()),this,SLOT(endConnectCard()));
                connect(timerForHide,SIGNAL(timeout()),this,SLOT(hideXterm()));

                QString prg_pin = "/etc/scripts/callRequestPIN.sh";
                arguList->append(ftpPassword);
                arguList->append(pathLine->text());
                arguList->append(QDir::homePath()+"/result.txt");
                monProgPIN->startDetached(prg_pin,*arguList);

                emit textToMainWindow("Connecting to VPN then FTP...");

                // check result
                timer->start(12000);
                timerForHide->start(3000);
            }
        }
    } else if (reply == QMessageBox::No){
        // OTP
        otpLogin->exec();

        if (flagLogin) {
            QTimer* timer = new QTimer;
            QTimer* timerForHide = new QTimer;
            timer->setSingleShot(true);
            timerForHide->setSingleShot(true);
            connect(timer,SIGNAL(timeout()),this,SLOT(endConnectOTP()));
            connect(timerForHide,SIGNAL(timeout()),this,SLOT(hideXterm()));

            // exec openvpn with otp script
            QProcess *monProgOTP = new QProcess();
            QStringList *arguList = new QStringList;
            QString prg_pexpect = "/etc/scripts/callRequestOTP.sh";
            arguList->append(ftpLogin);
            arguList->append(ftpPassword + ftpOtp);
            arguList->append(pathLine->text());
            arguList->append(QDir::homePath()+"/result.txt");
            monProgOTP->startDetached(prg_pexpect,*arguList);
            emit textToMainWindow("Connecting to VPN then FTP...");

            // check result
            timer->start(10000);
            timerForHide->start(3000);
        }
    }
}

void FilesView::hideXterm(){
    QProcess *monProgHide = new QProcess();
    monProgHide->execute("/etc/scripts/hideXterm.sh \"null\"");
}

void FilesView::setFlagInsertCard(){
    flagCard = false;
}

// SLOT : called 12 sec after enter PIN
void FilesView::endConnectCard(){
    QFile inputFile(QDir::homePath()+"/result.txt");
    if (inputFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&inputFile);
        QString line = in.readLine();
        inputFile.close();
        switch(line.toInt()){
            case 2: QMessageBox::information(this,"Error","Bad host or reinsert your card");
                    break;
            case 1: QMessageBox::information(this,"Error","Bad PIN");
                    break;
            case 0: if (flagLogin) {
                        QUrl url = QString("ftp://%1:%2@10.8.1.1").arg(ftpLogin).arg(ftpPassword);
                        ftpModel->setUrl(url);
                    }
                    flagLogin = false;
                    ftpLogin = "";
                    ftpPassword = "";
                    break;
            default: QMessageBox::information(this,"Error","Error");
                    break;
        }
    }
}

// SLOT : called 10 sec after enter OTP
void FilesView::endConnectOTP(){
    QFile inputFile(QDir::homePath()+"/result.txt");
    if (inputFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&inputFile);
        QString line = in.readLine();
        inputFile.close();
        switch(line.toInt()){
            case 2: QMessageBox::information(this,"Error","Bad host");
                    break;
            case 1: QMessageBox::information(this,"Error","Bad OTP");
                    break;
            case 0: if (flagLogin) {
                        QUrl url = QString("ftp://%1:%2@10.8.2.1").arg(ftpLogin).arg(ftpPassword);
                        ftpModel->setUrl(url);
                    }
                    flagLogin = false;
                    ftpLogin = "";
                    ftpPassword = "";
                    break;
            default: QMessageBox::information(this,"Error","Error");
                    break;
        }
    }
}

// SLOT : when button ok in logindialog is clicked : update var
void FilesView::slotAcceptUserLogin(QString login, QString password, QBool flag) {
    flagLogin = flag;
    ftpLogin = login;
    ftpPassword = password;
}

// SLOT : when button ok in logindialog is clicked : update var
void FilesView::slotAcceptUserLogin(QString login, QString password, QString otp, QBool flag) {
    flagLogin = flag;
    ftpLogin = login;
    ftpPassword = password;
    ftpOtp = otp;
}

// SLOT : display message
void FilesView::textFromFtpModel(QString str) {
    emit textToMainWindow(str);
}

// SLOT : FTP auto deconnect (enable button)
void FilesView::disconnectFromFtpModel() {
    buttonConnect->setEnabled(true);
    pathLine->setEnabled(true);
    emit disconnected();
}

// SLOT : FTP connect (enable button)
void FilesView::connectFromFtpModel() {
    buttonConnect->setEnabled(false);
    pathLine->setEnabled(false);
    loggedIn = true;
    emit connected();
}

// SLOT : FTP logged
void FilesView::notLoggedFromFtpModel() {
    buttonConnect->setEnabled(false);
    pathLine->setEnabled(false);
}

// FUNCTION : get FTP files list
QSet<QString> FilesView::downloadList(){
    QSet<QString> ret;
    QMap<QString, QString>::iterator it;
    for (it = ftpMap.begin(); it != ftpMap.end(); ++it) {
        ret.insert(it.value());
    }
    return ret;
}

// FUNCTION : call ftpmodel with FTP files list
void FilesView::callDownload(QSet<QString> se){
    ftpModel->Ftp_Down_Data(se);
}

// FUNCTION : get local files list
QSet<QString> FilesView::UploadList(){
    QSet<QString> ret;
    foreach (QFileInfo info, localMap.values()) {
        ret.insert(info.absoluteFilePath());
    }
    return ret;
}

// FUNCTION : call ftpmodel with local files list
void FilesView::upload(QSet<QString> se){
    ftpModel->Upload(se);
}

// FUNCTION : reset local list
void FilesView::EmptyUserList(){
    localMap.clear();
    list->clear();
}

// SLOT : progress bar transfer
void FilesView::TransfertProgressFromFtpModel(qint64 done, qint64 total, QList<QString> *filesName, bool flag) {
    if (!filesName->isEmpty()) {
        if (progressBar->getFile() != filesName->first()) {
            progressBar->close();
            progressBar = new ProgressBar(filesName->first());
            progressBar->show();

            progressBar->setFixedSize(progressBar->size());
            QPoint p = loginDialog->pos();
            p.setX(loginDialog->pos().x() - (loginDialog->width() / 2));
            progressBar->move(mapToGlobal(p));
        }
        progressBar->total(total);
        progressBar->done(done);
    } else {
        progressBar->close();
        if (flag) {
            ftpModel->refresh(tree->rootIndex());
        }
    }
}

// FUNCTION : reset local list
void FilesView::EmptyFtprList(){
    ftpMap.clear();
    list->clear();
}
