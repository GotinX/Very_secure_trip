/*
*   Names: Hamza
*   Date: 22/01/2014
*/

#include <QtGui/QApplication>
#include <QObject>
#include <QtCrypto>
#include <QDebug>
#include <QProcess>
#include <QSplashScreen>
#include <QTimer>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    // On initialise QCA, ce qui permettra de nettoyer la mémoire
    // en fin de programme
    QCA::Initializer init = QCA::Initializer();

    QCA::SecureArray tenBytes(10);
    tenBytes = QCA::Random::randomArray(10);

    /* for future...
    QSplashScreen *splash = new QSplashScreen;
    QObject::connect(monProg,SIGNAL(finished(int)),splash,SLOT(close()));
    QObject::connect(monProg,SIGNAL(finished(int)),&w,SLOT(show()));
    splash->setPixmap(QPixmap(":/splashPNG"));
    splash->show();
    */

    // initialisation
    QProcess *monProg = new QProcess();
    monProg->execute("python /etc/scripts/autoConfigEncfs.py " +
                     QDir::homePath() + "/.workspace " +
                     QDir::homePath() + "/workspace " +
                     QCA::Hex().arrayToString(tenBytes).toAscii().data());
    MainWindow w;
    w.show();
    return a.exec();
}
