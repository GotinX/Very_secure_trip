/*
*   Names: Hamza
*   Date: 27/01/2014
*/

#include "otplogindialog.h"

// CONSTRUCTOR
OTPLoginDialog::OTPLoginDialog(QWidget *parent) :
    QDialog(parent) {

    setUpGUI();
    setWindowTitle( tr("OTP Authentication") );
    setModal(true);
}

// FUNCTION : create login pop up
void OTPLoginDialog::setUpGUI() {
    // set up the layout
    QGridLayout* formGridLayout = new QGridLayout(this);
    // initialize the username combo box so that it is editable
    editUsername = new QLineEdit(this);
    editUsername->setText("");
    editPassword = new QLineEdit(this);
    editPassword->setEchoMode(QLineEdit::Password);
    editPassword->setText("");
    editOTP = new QLineEdit(this);
    editOTP->setValidator(new QIntValidator(0, 99999999, this) );
    editOTP->setText("");

    // initialize buttons
    buttons = new QDialogButtonBox(this);
    buttons->addButton( QDialogButtonBox::Ok );
    buttons->addButton( QDialogButtonBox::Cancel );
    buttons->button(QDialogButtonBox::Ok)->setText(tr("Login"));
    buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
    buttons->button(QDialogButtonBox::Cancel)->setText( tr("Abort") );

    // connects slots
    connect(buttons->button( QDialogButtonBox::Cancel ), SIGNAL(clicked()), this, SLOT(close()));
    connect(editUsername, SIGNAL(textChanged(QString)), this, SLOT(loginChanged(QString)));
    connect(editPassword, SIGNAL(textChanged(QString)), this, SLOT(loginChanged(QString)));
    connect(editOTP, SIGNAL(textChanged(QString)), this, SLOT(loginChanged(QString)));
    connect(buttons->button( QDialogButtonBox::Ok ), SIGNAL(clicked()),this, SLOT(slotAcceptLogin()));

    // place components into the dialog
    formGridLayout->addWidget(new QLabel("Username"), 0, 0 );
    formGridLayout->addWidget(editUsername, 0, 1 );
    formGridLayout->addWidget(new QLabel("Password"), 1, 0 );
    formGridLayout->addWidget(editPassword, 1, 1 );
    formGridLayout->addWidget(new QLabel("OTP"), 2, 0 );
    formGridLayout->addWidget(editOTP, 2, 1 );
    formGridLayout->addWidget(buttons, 3, 0, 1, 3 );
    setLayout(formGridLayout);
}

// SLOT : when button 'ok' is clicked : get username/password and send login (fileView)
void OTPLoginDialog::slotAcceptLogin() {
    QString username = editUsername->text();
    QString password = editPassword->text();
    QString otp = editOTP->text();
    emit acceptLogin(username, password, otp, QBool(true));
    editUsername->setText("");
    editPassword->setText("");
    editOTP->setText("");
    this->close();
}

// SLOT : check if username and password is not empty (enable button ok)
void OTPLoginDialog::loginChanged(QString str) {
    str = "";
    if (editUsername->text().isEmpty() || editPassword->text().isEmpty() || editOTP->text().isEmpty()) {
        buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
    } else {
        buttons->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}

