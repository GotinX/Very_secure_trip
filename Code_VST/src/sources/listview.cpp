/*
*   Names: Hamza
*   Date: 24/01/2014
*/

#include "listview.h"

// CONSTRUCTOR
ListView::ListView() {
}

// FUNCTION : reimplemented mouse press (right clic)
void ListView::mousePressEvent(QMouseEvent *event) {
    QListView::mousePressEvent(event);
    if(event->button() == Qt::RightButton) {
        emit customContextMenuRequested(event->pos());
    }
}
