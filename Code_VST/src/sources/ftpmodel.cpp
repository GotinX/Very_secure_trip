#include "ftpmodel.h"

#include <QtAlgorithms>
#include <qlocale.h>
#include <qdirmodel.h>
#include <qmimedata.h>
#include <qdebug.h>
#include <QDirIterator>
#include <QMessageBox>
#include <QProcess>

class FtpItem {
public:
    FtpItem() : fetchedChildren(false), parent(0) {}
    inline bool isDir() const { return info.isDir(); }
    QUrlInfo info;
    bool fetchedChildren;
    QList<FtpItem> children;
    FtpItem *parent;
    inline bool operator <(const FtpItem &item) const { return item.info.name() < info.name(); }
};

// CONSTRUCTOR
FtpModel::FtpModel(QObject *parent) : QAbstractItemModel(parent) {
    connect(&connection, SIGNAL(dataTransferProgress(qint64,qint64)), this, SLOT(ftpProgress(qint64,qint64)));
    connect(&connection, SIGNAL(listInfo(const QUrlInfo &)),this, SLOT(gotNewListInfo(const QUrlInfo &)));
    connect(&connection, SIGNAL(stateChanged(int)), this, SLOT(stateChanged(int)));
    connect(&connection, SIGNAL(commandFinished(int, bool)),this, SLOT(commandFinished(int, bool)));
    connect(&connection, SIGNAL(commandStarted(int)), this, SLOT(commandStarted(int)));
    connect(&connection, SIGNAL(listInfo(QUrlInfo)),this, SLOT(slotListInfo(QUrlInfo)));

    root = new FtpItem();
    iconProvider = new QFileIconProvider();
    filters = QDir::Readable | QDir::Writable | QDir::Executable | QDir::NoDotAndDotDot;
    actualTransferedFile = "";
    flagProgressBar = false;
    copyExist = false;
}

// DESTRUCTOR
FtpModel::~FtpModel() {
    delete root;
    delete iconProvider;
}

// FUNCTION: Returns number of childrens of parent
int FtpModel::rowCount(const QModelIndex &parent) const {
    if (!connected() || parent.column() > 0) {
        return 0;
    }
    return ftpItem(parent)->children.count();
}

// FUNCTION: Returns number of columns (4) of parent
int FtpModel::columnCount(const QModelIndex &parent) const {
    if (parent.column() > 0) {
        return 0;
    }
    return 4;
}

// FUNCTION: Check if parent has sub files or directories otherwise false
bool FtpModel::hasChildren(const QModelIndex &parent) const {
    if (!connected() || connection.state() != QFtp::LoggedIn) {
        return false;
    }
    const FtpItem *item = ftpItem(parent);
    if (!parent.isValid()) {
        return (item->children.count() > 0);
    }
    return item->isDir();
}

// FUNCTION: Returns true if parent has sub files or directories otherwise false
bool FtpModel::canFetchMore(const QModelIndex &parent) const {
    if (!connected() || connection.state() != QFtp::LoggedIn) {
        return false;
    }
    const FtpItem *item = ftpItem(parent);
    return (!item->fetchedChildren);
}

// FUNCTION: Get childrens of directory parent
void FtpModel::fetchMore(const QModelIndex &parent) {
    if (!connected() || connection.state() != QFtp::LoggedIn) {
        connection.close();
        emit stateChanged(5);
        return;
    }

    FtpItem *item = parent.isValid() ? static_cast<FtpItem*>(parent.internalPointer()) : root;
    if (item->fetchedChildren) {
        return;
    }

    item->fetchedChildren = true;
    QString fullPath = filePath(parent);
    int listCommand = connection.list(fullPath);
    listing.append(fullPath);
    listingCommands.append(listCommand);
}

// SLOT: Called after FTP listing command
void FtpModel::slotListInfo(QUrlInfo tt) {
    Q_UNUSED(tt);
}

QVariant FtpModel::data(const QModelIndex &index, int role) const {
    if (!connected() || !index.isValid()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch (index.column()) {
        case 0:
            return fileName(index);
        case 1:
            return size(index);
        case 2:
            return type(index);
        case 3:
            return time(index);
        }
        break;
    case Qt::DecorationRole:
        if (index.column() == 0) {
            return fileIcon(index);
        }
        break;
    case Qt::TextAlignmentRole:
        if (index.column() == 1) {
            return Qt::AlignRight;
        }
        break;
    }

    return QVariant();
}

// FUNCTION: The text representation of the binary size of index
QString FtpModel::size(const QModelIndex &index) const {
    if (!connected()) {
        return QString();
    }
    const FtpItem *item = ftpItem(index);
    if (item->isDir()) {
        return "";
    }

    quint64 bytes = item->info.size();

    if (bytes >= 1000000000) {
        return QLocale().toString(bytes / 1000000000) + QString(" GB");
    }

    if (bytes >= 1000000) {
        return QLocale().toString(bytes / 1000000) + QString(" MB");
    }

    if (bytes >= 1000) {
        return QLocale().toString(bytes / 1000) + QString(" KB");
    }

    return QLocale().toString(bytes) + QString(" bytes");
}

// FUNCTION: Returns the type of index (folder, file)
QString FtpModel::type(const QModelIndex &index) const {
    if (!connected()) {
        return QString();
    }
    const FtpItem *item = ftpItem(index);
    if (item->isDir()) {
        return tr("Folder");
    }

    return tr("File");
}

// FUNCTION: Returns last modification time of index
QString FtpModel::time(const QModelIndex &index) const {
    if (!connected()) {
        return QString();
    }
    const FtpItem *item = ftpItem(index);

#ifndef QT_NO_DATESTRING
    return item->info.lastModified().toString(Qt::LocalDate);
#else
    return QString();
#endif
}

// FUNCTION: Returns the flags of the index
Qt::ItemFlags FtpModel::flags(const QModelIndex &index) const {
    if (!connected()) {
        return 0;
    }
    if (!index.isValid()) {
        return Qt::ItemIsDropEnabled;

    }

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    flags |= Qt::ItemIsDragEnabled;
    if (index.column() == 0) {
        const FtpItem *item = ftpItem(index);
        if (item->info.isWritable()) {
            flags |= Qt::ItemIsEditable;
        }
        if (item->isDir() && item->info.isWritable()) {
            flags |= Qt::ItemIsDropEnabled;
        }
    }
    return flags;
}

// FUNCTION: Returns headers data for the view
QVariant FtpModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case 0: return tr("Name");
            case 1: return tr("Size");
            case 2: return tr("Type");
            case 3: return tr("Date Modified");
            default : return QVariant();
        }
    }
    return QAbstractItemModel::headerData(section, orientation, role);
}

// FUNCTION: Returns the parent of an index
QModelIndex FtpModel::parent(const QModelIndex &index) const {
    if (!connected() || !index.isValid()) {
        return QModelIndex();
    }
    const FtpItem *item = static_cast<const FtpItem*>(index.internalPointer());
    if (!item->parent || item->parent == (root)) {
        return QModelIndex();
    }
    const FtpItem *grandparent = item->parent->parent;
    if (!grandparent) {
        return QModelIndex();
    }

    for (int i = 0; i < grandparent->children.count(); ++i) {
        if (&(grandparent->children.at(i)) == item->parent) {
            return createIndex(i, 0, item->parent);
        }
    }
    return QModelIndex();
}

QModelIndex FtpModel::index(int row, int column, const QModelIndex &parent) const {
    if (!connected() || !hasIndex(row, column, parent)) {
        return QModelIndex();
    }
    const FtpItem *parentItem = ftpItem(parent);
    return createIndex(row, column, (void*)(&(parentItem->children[row])));
}

// FUNCTION: Returns the model index for a given path.
QModelIndex FtpModel::index(const QString &path, int column) const {
    if (!connected() || column < 0 || column >= columnCount() || path.isEmpty()) {
        return QModelIndex();
    }

    QStringList builtPath = path.split("/", QString::SkipEmptyParts);
    FtpItem *item = root;

    int r = 0;
    while (!builtPath.isEmpty()) {
        bool found = false;
        for (int i = 0; i < item->children.count(); ++i) {
            if (item->children.at(i).info.name() == builtPath.first()) {
                item = &(item->children[i]);
                builtPath.takeFirst();
                r = i;
                found = true;
                break;
            }
        }

        if (!found) {
            return QModelIndex();
        }
    }
    return createIndex(r, column, item);
}

// FUNCTION: Returns the file name for the item stored at index
QString FtpModel::fileName(const QModelIndex &index) const {
    if (!connected()) {
        return QString();
    }
    return ftpItem(index)->info.name();
}

// FUNCTION: Returns true if the item stored at index is a directory otherwise false
bool FtpModel::isDir(const QModelIndex &index) const {
    if (!connected()) {
        return false;
    }
    return ftpItem(index)->isDir();
}

// FUNCTION: Returns the filter used on directory listing
QDir::Filters FtpModel::filter() const {
    return filters;
}

// FUNCTION: Setting the filter used on directory listing
void FtpModel::setFilter(QDir::Filters filters) {
    this->filters = filters;
}

// FUNCTION: Refreshing the directory at parent
void FtpModel::refresh(const QModelIndex &parent) {
    if (!connected() ) { //|| !isDir(parent)
        return;
    }

    FtpItem *item = parent.isValid() ? static_cast<FtpItem*>(parent.internalPointer()) : root;
    if(!item->children.isEmpty()) {
        beginRemoveRows(parent, 0, item->children.count() - 1);
        item->children.clear();
        endRemoveRows();
    }
    item->fetchedChildren = false;
    fetchMore(QModelIndex());
}

// FUNCTION: Returns the default system icon for the index
QIcon FtpModel::fileIcon(const QModelIndex &index) const {
    if (!connected() || !index.isValid()) {
        return QIcon();
    }
    const FtpItem *item = ftpItem(index);
    if (item->isDir()) {
        return iconProvider->icon(QFileIconProvider::Folder);
    }

    return iconProvider->icon(QFileIconProvider::File);
}

// FUNCTION: Returns the full path for the item stored at index
QString FtpModel::filePath(const QModelIndex &index) const {
    if (!connected()) {
        return QString();

    }
    const FtpItem *item = ftpItem(index);
    QStringList path;
    while (item && item != root) {
        path.prepend(item->info.name());
        item = item->parent;
    }

    return path.join("/");
}

// FUNCTION: Remove rows from the model
bool FtpModel::removeRows(int row, int count, const QModelIndex &parent) {
    if (!connected() || count < 1 || row < 0 || (row + count) > rowCount(parent)) {
        return false;
    }

    FtpItem *item = parent.isValid() ? static_cast<FtpItem*>(parent.internalPointer()) : root;
    beginRemoveRows(parent, row, row + count - 1);

    for (int r = 0; r < count; ++r) {
        item->children.removeAt(row);
    }

    endRemoveRows();
    return true;
}

Qt::DropActions FtpModel::supportedDropActions() const {
    if (!connected()) {
        return 0;
    }

    return Qt::CopyAction | Qt::MoveAction;
}


QStringList FtpModel::mimeTypes() const {
    return QStringList(QLatin1String("text/uri-list"));
}

QMimeData *FtpModel::mimeData(const QModelIndexList &indexes) const {
    if (!connected()) {
        return 0;
    }
    QList<QUrl> urls;
    for (int i = 0; i <indexes.count(); ++i) {
        if (indexes.at(i).column() != 0) {
            continue;
        }
        QUrl url = ftpUrl;
        url.setPath(filePath(indexes.at(i)));
    }

    QMimeData *data = new QMimeData();
    data->setUrls(urls);
    return data;
}

bool FtpModel::dropMimeData(const QMimeData *data, Qt::DropAction action,int row, int column, const QModelIndex &parent) {
    if (!connected() || (action != Qt::CopyAction && action != Qt::MoveAction)) {
        return false;
    }

    QString to = filePath(index(row, column, parent)) + QDir::separator();
    QList<QUrl> urls = data->urls();

    for (int i = 0; i < urls.count(); ++i) {
        QFile *file = new QFile(urls.at(i).path());
        file->open(QIODevice::ReadOnly);
        int cmd = connection.put(file, to + file->fileName());

        if (action == Qt::CopyAction) {
            copyCommands[cmd] = file;
        } else {
            moveCommands[cmd] = file;
        }
    }
    return true;
}

// FUNCTION: Recursively sorting parent
void FtpModel::sort(FtpItem *parent, Qt::SortOrder order) {
    qSort(parent->children);
    int childCount = parent->children.count();
    if (Qt::AscendingOrder != order) {
        for (int i = 0; i < childCount / 2; ++i) {
            parent->children.swap(i, childCount - i - 1);
        }
    }

    for (int i = 0; i < childCount; ++i) {
        sort(&parent->children[i], order);
    }
}

// FUNCTION: Reimplementation of sort
void FtpModel::sort(int column, Qt::SortOrder order) {
    Q_UNUSED(column);
    if (!connected()) {
        return;
    }

    sort(root, order);
    emit layoutChanged();
}

// FUNCTION: Setting the FTP host and port
void FtpModel::setUrl(const QUrl &url) {
    ftpUrl = url;
    connection.connectToHost(url.host(), url.port(2121));
}

// FUNCTION: Returns the FTP host
QUrl FtpModel::url() const {
    return ftpUrl;
}

// SLOT: Called after FTP listing command
void FtpModel::gotNewListInfo(const QUrlInfo &info) {
    if (!connected())
        return;
    if (info.isExecutable() && !(filters & QDir::Executable))
        return;
    if (info.isReadable() && !(filters & QDir::Readable))
        return;
    if (info.isWritable() && !(filters & QDir::Writable))
        return;
    if (info.isSymLink() && filters & QDir::NoSymLinks)
        return;
    if (info.isDir() && filters & QDir::Files)
        return;
    if ((info.name() == "." || info.name() == "..") && filters & QDir::NoDotAndDotDot)
        return;
    if (info.name().at(0) == '.' && !(filters & QDir::Hidden))
        return;

    QModelIndex idx = index(listing.first());
    beginInsertRows(idx, rowCount(idx), rowCount(idx));
    FtpItem *parentItem = idx.isValid() ? static_cast<FtpItem*>(idx.internalPointer()) : root;
    FtpItem item;
    item.parent = parentItem;
    item.info = info;
    item.fetchedChildren = !info.isDir();
    parentItem->children.append(item);
    endInsertRows();
}

// SLOT: Called when FTP state changed
void FtpModel::stateChanged(int state) {
    if (!connected() && root->fetchedChildren) {
        delete root;
        root = new FtpItem();
        reset();
    }

    QString stateString;
    switch (state) {
        case 0: stateString = "Unconnected";
            emit disconnectToFilesView();
            break;
        case 1: stateString = "HostLookup";
            break;
        case 2: stateString = "FTP connecting...";
            emit notLoggedToFilesView();
            break;
        case 3: stateString = "VPN/FTP connected";
            connection.login(ftpUrl.userName(), ftpUrl.password());
            emit notLoggedToFilesView();
            break;
        case 4: stateString = "Welcome " + ftpUrl.userName();
            emit connectToFilesView();
            fetchMore(QModelIndex());
            break;
        case 5: stateString = "Closing";
            break;
        default:
            stateString = "New state: " + state;
    }
    emit textToFilesView(stateString);
}

// SLOT: Called when FTP command is started
void FtpModel::commandStarted(int id) {
    Q_UNUSED(id);
}

// SLOT: Called when FTP command is finished
void FtpModel::commandFinished(int id, bool error) {
    QFtp::Command cmd = connection.currentCommand();

    if (cmd == QFtp::ConnectToHost) {
        if (error) {
            emit textToFilesView("Unable to connect to the FTP server. Please check that the host adress is correct.");
            return;
        }
    }
    if (error) {
        emit textToFilesView("Error : " + connection.errorString());
        connection.close();
        //emit connection.stateChanged(5);
    }

    if(cmd == QFtp::Get){
        QRegExp rxfind(".zip$");
        QRegExp rxreplace("[^/]*.zip$");
        QProcess *monProg = new QProcess();
        QString file_name = fichiers.value(connection.currentId())->fileName();
        QString path_file = file_name;
        fichiers.value(connection.currentId())->close();

        if (file_name.contains(rxfind)) {
            path_file.replace(rxreplace,"");
            emit textToFilesView("Uncompressing data...");
            monProg->execute("unzip -o " + file_name + " -d " + path_file);
            monProg->waitForFinished();
            monProg->execute("rm " + file_name);
        }

        delete fichiers.value(connection.currentId());
    }
    if(cmd == QFtp::Put){
        QRegExp rxfind(".piz$");
        fichiers.value(connection.currentId())->close();
        QString file_name = fichiers.value(connection.currentId())->fileName();
        QProcess *monProg = new QProcess();
        if (file_name.contains(rxfind)) {
            monProg->execute("rm " + file_name);
        }
        delete fichiers.value(connection.currentId());
    }

    // pour affichage
    if (!listingCommands.isEmpty() && listingCommands.first() == id) {
        listing.pop_front();
        listingCommands.pop_front();
    }
}

// FUNCTION: Remove FTP file or directory
int FtpModel::removeToFtp(QString sPath, bool isDir) {
    if(isDir) {
        return connection.rmdir(sPath);
    }
    return connection.remove(sPath);
}

// FUNCTION: Close FTP connection
void FtpModel::close() {
    connection.close();
}

// FUNCTION: Change FTP directory
void FtpModel::Ftp_Ch_Dir(QString dir) {
    connection.cd(dir);
}

// FUNCTION: Upload fonction call UploadFile (compress file/folder)
void FtpModel::Upload(QSet <QString> set) {
    filesBeingTransfered = new QList<QString>(set.toList());
    flagProgressBar = true;

    foreach(QString file_rep , set) {
        QFileInfo file = file_rep;
        QProcess *monProg = new QProcess();
        emit textToFilesView("Compressing data...");
        QDir::setCurrent(QDir::homePath()+"/workspace");
        QString dst = file.absoluteFilePath();
        QRegExp rxreplace("^"+QDir::homePath()+"/workspace");
        dst.replace(rxreplace,"");
        dst.replace(0,1,"");
        monProg->execute("zip -r " + dst+".piz " + dst);
        UploadFile(dst+".piz");
    }
}

// FUNCTION: Upload file
void FtpModel::UploadFile(QString s) {
    int id;
    if (!connected() || connection.state() != QFtp::LoggedIn) {
        connection.close();
        emit stateChanged(5);
        return;
    }

    QFileInfo file;
    QFile *se = new QFile(s);
    file.setFile(s);
    id = connection.put(se, file.fileName());
    fichiers[id]=se;
}

/* MAJ : utilisation de la compression
// FUNCTION: Upload directory recursively
void FtpModel::UploadDirectory(QDir dir, QString base) {
    connection.mkdir(dir.dirName());

    QDirIterator iterator(dir.absolutePath(), QDirIterator::Subdirectories);
    while (iterator.hasNext()) {
        iterator.next();
        if (iterator.fileInfo().isDir() && iterator.fileInfo().fileName() != "." && iterator.fileInfo().fileName() != "..") {
            QString filename = iterator.filePath().mid(QString(base).size());
            QString filen = iterator.fileName();
            foreach (QString s, filename.split("/")) {
                if (filename.split("/").last() != s) {
                    connection.cd(s);
                }
            }

            connection.mkdir(filen);

            for (int i = 0; i < filename.count("/"); i++) {
                connection.cd("..");
            }
        }
    }

    QDirIterator fileIterator(dir.absolutePath(), QDirIterator::Subdirectories);
    while (fileIterator.hasNext()) {
        fileIterator.next();
        if (!fileIterator.fileInfo().isDir()) {
            QString filename = fileIterator.filePath().mid(QString(base).size());
            QString filen = fileIterator.fileName();

            qDebug() << filename;

            foreach (QString s, filename.split("/")) {
                if (filename.split("/").last() != s) { // Ici Bug si dossier parent a le même nom que le fichier
                    connection.cd(s);
                }
            }

            connection.put(new QFile(fileIterator.filePath()), filen);

            for (int i = 0; i < filename.count("/"); i++) {
                connection.cd("..");
            }
        }
    }
}
*/

// FUNCTION: Download
void FtpModel::Ftp_Download(QString fil, bool flag) {
    int id;
    QFileInfo f(fil);
    QFile *fi = new QFile;
    // on stocke le fichier choisis avec comme racine le home
    if(flag){
        fi->setFileName(QDir::homePath()+"/workspace" + "/" + fil + ".zip");
        fi->open(QIODevice::WriteOnly);
        id = connection.get(f.filePath()+".zip",fi);
    } else {
        fi->setFileName(QDir::homePath()+"/workspace" + "/" + fil);
        fi->open(QIODevice::WriteOnly);
        id = connection.get(f.filePath(),fi);
    }
    fichiers[id]=fi;
}

// FUNCTION: Download main
void FtpModel::Ftp_Down_Data(QSet <QString> set) {
    filesBeingTransfered = new QList<QString>(set.toList());
    flagProgressBar = false;

    QDir dloc = QDir::homePath()+"/workspace";
    foreach(QString file_rep , set){
        QFileInfo file;
        file.setFile(file_rep);
        dloc.mkpath(file.path());
        if(isDir(index(file_rep))){
            Ftp_Download(file.filePath(),true);
        } else {
            Ftp_Download(file.filePath(),false);
        }
    }
}

// SLOT: Transfer progression
void FtpModel::ftpProgress(qint64 done, qint64 total) {
    if (done == total) {
        filesBeingTransfered->removeFirst();
    }
    emit TransfertProgress(done, total, filesBeingTransfered, flagProgressBar);
}

void FtpModel::setCopy(bool cc){
    copyExist = cc;
}

bool FtpModel::getCopy(){
    return copyExist;
}

void FtpModel::setCutMemory(QString file){
    cutMemory = file;
}

void FtpModel::cutAndCopy(QString dst){
    QFileInfo f(cutMemory);
    connection.rename(cutMemory,dst+f.fileName());
    cutMemory = dst+f.fileName();
    refresh();
}
