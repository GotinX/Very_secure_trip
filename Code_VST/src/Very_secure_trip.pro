#-------------------------------------------------
#
# Project created by QtCreator 2014-01-21T14:14:39
#
#-------------------------------------------------

QT      += core gui
QT      += network

CONFIG += crypto

RESOURCES    += resources.qrc

TARGET = Very_secure_trip
TEMPLATE = app

INCLUDEPATH += ./sources
INCLUDEPATH += ./headers

DESTDIR = ../bin/
OBJECTS_DIR = ../bin/
MOC_DIR = ../bin/

SOURCES += sources/main.cpp\
    sources/mainwindow.cpp \
    sources/filesview.cpp \
    sources/listview.cpp \
    sources/ftpmodel.cpp \
    sources/progressbar.cpp \
    sources/otplogindialog.cpp \
    sources/logindialog.cpp

HEADERS  += headers/mainwindow.h \
    headers/filesview.h \
    headers/listview.h \
    headers/ftpmodel.h \
    headers/progressbar.h \
    headers/otplogindialog.h \
    headers/logindialog.h


