#ifndef OTPLOGINDIALOG_H
#define OTPLOGINDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>

class OTPLoginDialog : public QDialog {
    Q_OBJECT

private:
    QLineEdit* editUsername;
    QLineEdit* editPassword;
    QLineEdit* editOTP;
    QDialogButtonBox* buttons;
    void setUpGUI();

public:
    OTPLoginDialog(QWidget *parent = 0);

signals:
    void acceptLogin(QString username, QString password, QString otp, QBool flag);
    void closeToFilesView();

public slots:
    void slotAcceptLogin();

private slots:
    void loginChanged(QString str);

};

#endif // OTPLOGINDIALOG_H
