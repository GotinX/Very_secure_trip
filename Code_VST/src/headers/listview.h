#ifndef LISTVIEW_H
#define LISTVIEW_H

#include <QListWidget>
#include <QMouseEvent>

class ListView : public QListWidget
{
public:
    ListView();
    void mousePressEvent(QMouseEvent *event);
};

#endif // LISTVIEW_H
