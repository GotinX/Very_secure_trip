#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QProgressBar>
#include <QWidget>
#include <QString>

class ProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressBar(QString fileName = "", QWidget *parent = 0);

   private:
    QProgressBar *progressBar;
    QString fileName;
    
signals:
    
public slots:
    void total(quint64 total);
    void done(quint64 done);

public:
    QString getFile();
    
};

#endif // PROGRESSBAR_H
