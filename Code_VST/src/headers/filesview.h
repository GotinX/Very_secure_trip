#ifndef FILESVIEW_H
#define FILESVIEW_H

#include <QListView>
#include <QFileSystemModel>
#include <QLineEdit>
#include <QTreeView>
#include <QBool>
#include <QFileInfo>
#include <QMap>
#include <QListWidget>

#include "listview.h"
#include "otplogindialog.h"
#include "logindialog.h"
#include "ftpmodel.h"
#include "progressbar.h"

class FilesView : public QWidget {
    Q_OBJECT

public:
    FilesView(QBool isFtp, QString path, QWidget *parent = 0);
    QListView* getList();
    QTreeView* getTree();
    QFileSystemModel *getDirModel();
    QSet<QString> downloadList();
    void callDownload(QSet<QString>);
    void upload(QSet<QString> se);
    void EmptyUserList();
    void EmptyFtprList();
    QSet<QString>  UploadList();
    FtpModel *getFtpModel();

public slots:
    void OnTreeClicked(QModelIndex index);
    void OnTreeClickedFTP(QModelIndex index);
    void OnListClicked(QModelIndex index);
    void userListMenuRequested(QPoint pos);
    void OnListFtpClicked(QModelIndex index);
    void userListFtpMenuRequested(const QPoint & pos);
    void FtpConnect();
    void textFromFtpModel(QString str);
    void TransfertProgressFromFtpModel(qint64 done, qint64 total, QList<QString> *filesName, bool flag);

private:
    QFileSystemModel *fileModel;
    bool flagFTP;
    QLineEdit *pathLine;
    QPushButton *buttonConnect;
    //LoginDialog loginDialog;
    OTPLoginDialog* otpLogin;
    LoginDialog* loginDialog;
    QString ftpLogin;
    QString ftpPassword;
    QString ftpOtp;
    bool flagLogin;
    bool loggedIn;
    bool flagCard;

    QMap<QString, QString> ftpMap;
    QMap<QString, QFileInfo> localMap;
    ProgressBar *progressBar;
    QList<QString> *alreadyTransfered;

protected:
    ListView *list;
    QTreeView *tree;
    FtpModel *ftpModel;
    QFileSystemModel *dirModel;

private slots:
    void pathChanged(QString s);
    void slotAcceptUserLogin(QString login, QString password, QString otp, QBool flag);
    void slotAcceptUserLogin(QString login, QString password, QBool flag);
    void disconnectFromFtpModel();
    void connectFromFtpModel();
    void notLoggedFromFtpModel();
    void slotMenuTest(QPoint pos);
    void slotMenuTestFtp(QPoint pos);
    void endConnectCard();
    void endConnectOTP();
    void setFlagInsertCard();
    void hideXterm();

signals:
    void textToMainWindow(QString);
    void uploads(QSet<QString>);
    void disconnected();
    void connected();
    void TransfertProgressToMainWindow(qint64, qint64, QString);
};

#endif // FILESVIEW_H
