#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QWidget>
#include <QToolBar>
#include <QToolButton>
#include <QSet>
#include <QTextEdit>
#include <QPushButton>
#include <QProgressBar>

#include "filesview.h"
#include "progressbar.h"


class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setIsLogin(bool b);
    bool getIsLogin();


protected:
    QMenuBar *menuBar;
    QAction *getData;

private:
    void createView();
    void createMenuBar();
    void placeMenuBar();
    void placeComponents();
    void createModel();
    void createController();

    QWidget *centralWidget;
    FilesView *filesView1;
    FilesView *filesView2;
    QToolBar *toolBar;
    bool isLogin;

    QPushButton *buttonDownload;
    QPushButton *buttonUpload;
    QToolButton *buttonBrowser;
    QToolButton *buttonEditor;
    QToolButton *buttonQuit;
    QToolButton *buttonTerm;
    QToolButton *buttonPdf;

    QSet <QString> sortList;
    QSet <QString> sortListFtp;
    QTextEdit *debugText;

public slots:
    void openTerm();
    void openTermTest();
    void openBrowser();
    void openEditor();
    void openPdf();
    void quitter();
    void getConnected();
    void getDisconnected();
    void getDownloadList();

private slots:
    void TextFromFtp(QString str);
    void getUploadList();
    void about();
};

#endif // MAINWINDOW_H
