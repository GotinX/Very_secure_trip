#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>

class LoginDialog : public QDialog {
    Q_OBJECT

private:
    QLineEdit* editUsername;
    QLineEdit* editPassword;
    QDialogButtonBox* buttons;
    void setUpGUI();

public:
    LoginDialog(QWidget *parent = 0);

signals:
    void acceptLogin(QString username, QString password, QBool flag);
    void closeToFilesView();

public slots:
    void slotAcceptLogin();

private slots:
    void loginChanged(QString str);

};

#endif // LOGINDIALOG_H
