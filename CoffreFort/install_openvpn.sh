#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#installation de la librairie de développement pam et de openvpn
apt-get install libpam0g-dev libqrencode3 libqrencode-dev
apt-get install openvpn


#dézippe de tous les fichiers de configuration

cd google-authenticator/server
make
make install

#copie du fichier de vérification des comptes pour OpenVPN et OTP grâce à pam
cd ../pam.d
cp openvpn /etc/pam.d/

#Copie des fichiers de configuration de openvpn
sudo rm -r /etc/openvpn/*
cd ../config
cp -r * /etc/openvpn/
openssl dhparam -outform PEM -out /etc/openvpn/keys/dh1024.pem 1024
cd /usr/lib/openvpn/
echo plugin /usr/lib/openvpn/`ls openvpn*-auth-pam.so` openvpn >> /etc/openvpn/server-otp.conf



