#!/bin/bash
 
# Script iptables.
 
## Règles iptables.
 
## On flush iptables.
 
/sbin/iptables -F
 
## On supprime toutes les chaînes utilisateurs.
 
/sbin/iptables -X
 
## On drop tout le trafic entrant.
 
/sbin/iptables -P INPUT DROP
 
## On drop tout le trafic sortant.
 
/sbin/iptables -P OUTPUT DROP
 
## On drop le forward.
 
/sbin/iptables -P FORWARD DROP
 
## On drop les scans XMAS et NULL.
 
/sbin/iptables -A INPUT -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
 
/sbin/iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
 
/sbin/iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
 
/sbin/iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
 
## Dropper silencieusement tous les paquets broadcastés.
 
/sbin/iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP


## Permettre à une connexion ouverte de recevoir du trafic en entrée.
 
#/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
 
## Permettre à une connexion ouverte de recevoir du trafic en sortie.
 
/sbin/iptables -A OUTPUT -m state ! --state INVALID -j ACCEPT

## Permettre à une connexion sur les ports 1595, 1594 de recevoir du trafic en entrée
int=$(ifconfig | awk '{print $1}' | head -1)
/sbin/iptables -A INPUT -p tcp -i $int --dport 1595 -j ACCEPT
/sbin/iptables -A INPUT -p tcp -i $int --dport 1594 -j ACCEPT

## Permettre à une connexion VPN de recevoir du trafic en entrée
/sbin/iptables -A INPUT -i tun+ -j ACCEPT
/sbin/iptables -A FORWARD -i tun+ -j ACCEPT


## On accepte la boucle locale en entrée.
 
/sbin/iptables -I INPUT -i lo -j ACCEPT
 
## On log les paquets en entrée.
 
/sbin/iptables -A INPUT -j LOG
 
## On log les paquets forward.
 
/sbin/iptables -A FORWARD -j LOG 
 
exit 0
