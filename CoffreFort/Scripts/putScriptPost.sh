#!/bin/bash

dir=${3%.*}
base=$(basename $3)
path=$(dirname ${dir})
ext=${base##*.}

if [ "$ext" == "piz" ]; then
	unzip -o $3 -d $path

	if [ -e "$3" ]; then
		rm -rf $3
	fi
fi

exit 0
