#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [[ $1 == "--help" || $1 == "-h" ]]; then
    cat << EOF
Usage: 
    $0 user_login [commands]

[commands] are commands to install in chroot. If commands are not given, this basics commands will  be installed:
   - bash
   - basename
   - dirname
   - zip
   - rm
   - unzip
   - mkdir

Example:
    #create basic chroot
    $0 toto
    
    #create chroot with bash and perl
    $0 toto bash perl
    #add ls cat and python commands
    $0 toto ls cat python
EOF
exit 0 
fi

LOGIN=$1
GROUP=chroot
TARGET=/home/${LOGIN}

echo "-- Création du groupe \"${GROUP}\""
groupadd "${GROUP}" > /dev/null 2>&1

echo "-- Création de l'utilisateur \"${LOGIN}\""
useradd \
-c "User chrooted" \
-d "/home/${LOGIN}/" \
-g "${GROUP}" \
"${LOGIN}"

echo "-- Son mot de passe : "
passwd "${LOGIN}" > /dev/null


#Liste des binaires à déposer dans le chroot
if [[ $2 == "" ]]; then
    #si on a pas donné de commandes en paramètre ceci est la liste des
    #commandes par défaut
    BINS="bash basename dirname zip rm unzip mkdir"
else
    #sinon on prend la liste donnée
    shift
    BINS=$@
fi

if [[ "$TARGET" == "" ]];then 
    echo "target dirctory needed"
    exit 1
fi

#on vérifie que le chroot n'existe pas
if [[ -d $TARGET  ]]; then
    echo "$TARGET existe, voulez vous le vider avant de recréer le chroot (o/N)?"
    read del
    del=`echo $del | tr "[a-z]" "[A-Z]"`

    if [[ "$del" == "Y" || $del == "YES" || $del == "O" || $del == "OUI" ]]; then
        echo "Suppression de $TARGET"
        rm -rf $TARGET
    fi
fi
 

echo "Création du chroot en cours... merci de patienter"

#crée le répertoire et récupère le chemin absolu
mkdir -p $TARGET > /dev/null 2>&1
chown -R ${LOGIN}:${GROUP} $TARGET
pushd $TARGET > /dev/null 2>&1
TARGET=`pwd`
popd > /dev/null 2>&1

#pour chaque commande à copier
for basecommand in $BINS
do
    #recherche relative des binaire qui correspondent
    all=`whereis -b $basecommand | cut -f2 -d":"`
    for command in $all
    do
        #crée la cible et copie le binaire dans le chroot
        mkdir -p $TARGET`dirname $command` > /dev/null 2>&1
        cp -R -L "$command" $TARGET`dirname $command`

        #trouve les dépendances des bibliothèques et les copie dans
        #le choot
        for f in `ldd $command 2>/dev/null | cut -f2 -d ">" | cut -f1 -d "(" `
        do
            if [[ -f $f || -h $f ]];then
               mkdir -p $TARGET`dirname $f` > /dev/null 2>&1
               cp -R -L "$f" $TARGET`dirname $f`
            fi
        done
    done
done

#On crée le noeud /dev/null 
mkdir $TARGET/dev
mknod $TARGET/dev/null c 1 3

#et les périphériques de randomisation
mknod -m 0444 $TARGET/dev/random c 1 8
mknod -m 0444 $TARGET/dev/urandom c 1 9

#fini
echo "Chroot créé."
#echo "chroot $TARGET /bin/bash"

echo "Copie des scripts FTP ..."
cp ./getScript.sh $TARGET/.getScript.sh
cp ./getScriptPost.sh $TARGET/.getScriptPost.sh
cp ./putScriptPost.sh $TARGET/.putScriptPost.sh
cp ./removeDir.sh $TARGET/.removeDir.sh

chmod +x $TARGET/.*.sh

exit 0
