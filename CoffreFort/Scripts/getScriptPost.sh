#!/bin/bash

dir=${3%.*}
base=$(basename ${dir})
path=$(dirname ${dir})

if [ -d $dir ]; then
	rm -rf $3
fi
exit 0

