#!/bin/bash

dir=${3%.*}
base=$(basename ${dir})
path=$(dirname ${dir})

if [ -d $dir ]; then
	cd $path
	zip -r $base".zip" $base
fi
exit 0

