#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

dist=$(lsb_release -si)

if [ "$dist" != "Ubuntu" ]; then
   echo "This script was made for Ubuntu distribution only, use at your own risk"
   read "Press key to continue ..."
fi

echo "Installing ProFTP server ..."
# install build-tools and dependencies:
apt-get install build-essential gettext make g++ libwrap0-dev libldap2-dev libmysql++-dev libpam0g-dev libssl-dev libsqlite3-dev unixodbc-dev libncurses5-dev libacl1-dev libcap-dev wget

# download sources:
wget ftp://ftp.proftpd.org/distrib/source/proftpd-1.3.3e.tar.gz
tar -xvf proftpd-1.3.3e.tar.gz
cd proftpd-1.3.3e

# configure, make and install:
./configure --with-modules=mod_exec
make
make install

# cleaning
cd ..
rm proftpd-1.3.3e.tar.gz
rm -rf proftpd-1.3.3e

apt-get install proftpd

echo "Adding ProFTP server to the automatic startup system ..."
update-rc.d proftpd defaults

echo "Setting ProFTP server configuration ..."
cp proftpd.conf /etc/proftpd/proftpd.conf

echo "Restarting ProFTP server ..."
service proftpd restart

echo "ProFTP to add users use [Scripts/addUserFtp.sh]"

exit 0
