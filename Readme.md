# Very Secure Trip

## Présentation
L'objectif du projet est d'étudier les différents dispositifs pouvant être mis en place pour sécuriser les données contenues dans ces appareils, contre des vols et contre des passages de frontières.

On pourra en particulier s'intéresser aux points suivants:

* Chiffrement des données sur le disque ;
* Effacement sécurisé de données ;
* Mise en place d'un VPN avec authentification forte par carte à puce ;
* Coffrefort électronique pour du stockage en ligne de documents ;


## Prérequis
Paquets à installer (Ubuntu):

* git
* qtcreator
* build-essential 
* qt4-dev-tools 
* libqca2 
* libqca2-dev 
* libqca2-plugin-ossl 
* libqca2-plugin-gnupg

## Application cliente
### Architecture

Code_VST<br/>
├── /bin<br/>
├── /icons<br/>
├── /scripts<br/>
├── /src<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── /headers<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── /sources<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── Very\_secure\_trip.pro<br/>

### Compilation
```shell
[Code_VST/src]$ qmake-qt4 Very_secure_trip.pro -r -spec linux-g++ 
[Code_VST/src]$ make
[Code_VST/bin]$ ./Very_secure_trip
```

## Coffre fort
### Installation
```shell
# Installation du coffre fort 
[CoffreFort]$ ./safetybox_install.sh

# Création d'un utilisateur du coffre fort 
[CoffreFort/Scripts]$ ./addUserFtp.sh test_user
```

## Commandes Git
### Clone du projet
```shell
# Utilisation https (Université)
$ git config --global http.proxy http://user:pass@192.168.34.62:3128/
$ git clone https://gitlab.com/gh0st/Very_secure_trip.git

# Utilisation SSH
$ ssh-keygen -t rsa -b 4096 -C "adresse_mail@server.com"
$ git clone git@gitlab.com:gh0st/Very_secure_trip.git
```

### Configuration pour les commits
```shell
$ git config --global user.name "Prénom"
$ git config --global user.email "adresse_mail@server.com"
```

## Groupe de projet

* ANGO Charles-Erwan-Brou <angocharles@gmail.com>
* FIGUERES Mathieu        <matfig66@gmail.com>
* KABORE Ambroise Ismael  <ambroiseismael@gmail.com>
* ROSET Vincent           <vistrate360@gmail.com>
* ZERHOUNI ABDOU Hamza    <zerhounihamza@gmail.com>
